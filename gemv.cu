#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <sched.h>

using namespace std;

// #define FP16MM

const char* cublasGetErrorString(cublasStatus_t status)
{
    switch(status)
    {
        case CUBLAS_STATUS_SUCCESS: return "CUBLAS_STATUS_SUCCESS";
        case CUBLAS_STATUS_NOT_INITIALIZED: return "CUBLAS_STATUS_NOT_INITIALIZED";
        case CUBLAS_STATUS_ALLOC_FAILED: return "CUBLAS_STATUS_ALLOC_FAILED";
        case CUBLAS_STATUS_INVALID_VALUE: return "CUBLAS_STATUS_INVALID_VALUE"; 
        case CUBLAS_STATUS_ARCH_MISMATCH: return "CUBLAS_STATUS_ARCH_MISMATCH"; 
        case CUBLAS_STATUS_MAPPING_ERROR: return "CUBLAS_STATUS_MAPPING_ERROR";
        case CUBLAS_STATUS_EXECUTION_FAILED: return "CUBLAS_STATUS_EXECUTION_FAILED"; 
        case CUBLAS_STATUS_INTERNAL_ERROR: return "CUBLAS_STATUS_INTERNAL_ERROR"; 
    }
    return "unknown error";
}

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCuda(cudaError_t result)
{
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
  return result;
}

inline
cublasStatus_t checkCublas(cublasStatus_t result)
{
  if (result != CUBLAS_STATUS_SUCCESS) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cublasGetErrorString(result));
    assert(result == CUBLAS_STATUS_SUCCESS);
  }
  return result;
}

// Fill the array A(nr_rows_A, nr_cols_A) with random numbers on CPU
void CPU_fill_rand(float *A, int nr_rows_A, int nr_cols_A) {
	int a=1;

    for(int i = 0; i < nr_rows_A * nr_cols_A; i++){
		A[i] = (float)rand()/(float)(RAND_MAX/a);
	}
}

int main(int argc, char ** argv){
  std::cout << "Set scheduler..." << std::endl;
  struct sched_param sp = { .sched_priority = 49 };
  int ret;

  ret = sched_setscheduler(0, SCHED_FIFO, &sp);
  if (ret == -1) {
    perror("sched_setscheduler");
    return 1;
  }
  std::cout << "Scheduler set" << std::endl;
  int m = std::stoi(argv[1]);
  int n = std::stoi(argv[2]);
  // int m = 20000;
  // int n = 5072;
  int repeats = 1000000;
  int verbose = 1;

  // int min_m_k_n = 2;
  // int max_m_k_n = 4096*5;
  // int repeats = 20;
  // int verbose = 1;
  
  if(verbose) 
    cout << "running with" 
	 << " m: " << m
	 << " n: " << n
	 << " repeats: " << repeats
	 << endl;

  cublasStatus_t stat;
  cublasHandle_t handle;
  cudaSetDeviceFlags(cudaDeviceScheduleBlockingSync);
  checkCublas(cublasCreate(&handle));

  if(verbose) cout << "allocating device variables" << endl;
  
  // Allocate 3 arrays on CPU
  
  float *h_A = (float *)malloc(m * n * sizeof(float));
  float *h_B = (float *)malloc(n * sizeof(float));
  float *h_C = (float *)malloc(m * sizeof(float));
  
  CPU_fill_rand(h_A, m, n);
  CPU_fill_rand(h_B, n, 1);
  CPU_fill_rand(h_C, m, 1);

    // Allocate 3 arrays on GPU
    float *d_A, *d_B, *d_C;
    checkCuda(cudaMallocManaged(&d_A, m * n * sizeof(float)));
    checkCuda(cudaMallocManaged(&d_B, n * sizeof(float)));
    checkCuda(cudaMallocManaged(&d_C, m * sizeof(float)));
    
    checkCuda(cudaMemcpy(d_A,h_A,m * n * sizeof(float),cudaMemcpyHostToDevice));
    checkCuda(cudaMemcpy(d_B,h_B,n * sizeof(float),cudaMemcpyHostToDevice));
    checkCuda(cudaMemcpy(d_C,h_C,m * sizeof(float),cudaMemcpyHostToDevice));
    
    int lda, ldb, ldc;
    const float alf = 1.0f;
    const float bet = 0.0f;
    const float *alpha = &alf;
    const float *beta = &bet;
    lda = m;
    ldb = n;
    ldc = m;
  
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  stat = cublasSgemv(handle, CUBLAS_OP_N, m, n, &alf, d_A, lda, d_B, 1, &bet, d_C, 1);
    double sum = 0.0;
    double max = 0.0;
    double mini = 1e9;
    for(int rep = 0; rep < repeats; rep++){
      cudaEventRecord(start, 0);

      stat = cublasSgemv(handle, CUBLAS_OP_N, m, n, &alf, d_A, lda, d_B, 1, &bet, d_C, 1);
      cudaStreamSynchronize(0);
      cudaEventRecord(stop,0);
      cudaEventSynchronize(stop);
      if(stat != CUBLAS_STATUS_SUCCESS){
	cerr << "cublasSgemvBatched failed" << endl;
	exit(1);
      }
      assert(!cudaGetLastError());
      
      float elapsed;
      cudaEventElapsedTime(&elapsed, start, stop);
      elapsed /= 1000.0f;
      sum += elapsed;
      if(elapsed > max)
        max = elapsed;
      if(elapsed < mini)
        mini = elapsed;
    }
    cout <<  "average: " << sum/repeats << " s "<< endl;
    cout <<  "min: " << mini << " s "<< endl;
    cout <<  "max: " << max << " s "<< endl;

  

  //Free GPU memory
  cudaFree(d_A);
  cudaFree(d_B);
  cudaFree(d_C);

  // Free CPU memory
  free(h_A);
  free(h_B);
  free(h_C);
      
  return 0;
}
