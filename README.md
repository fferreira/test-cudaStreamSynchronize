### Procedure to reproduce results
1. Clone the repository
```bash
git clone https://gitlab.obspm.fr/fferreira/test-cudaStreamSynchronize.git
cd test-cudaStreamSynchronize
```
2. Modify first line of makefile with the right compute capabilities
3. Build and give capability to set scheduler :
```bash
make
sudo setcap 'cap_sys_nice=eip' /home/<username>/cublasgemm-benchmark/gemv
```
4. Launch on previouly isolated CPU (on boot is better)
```bash
tuna -c 10-15 -r "./gemv 25000 5000"
```
`numactl` can be also be used instead of `tuna`

5. Redo with the other version of CUDA

